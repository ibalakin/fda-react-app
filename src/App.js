import React from 'react';
import {Home} from './pages';
import {Header} from './components';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header/>
      <Home/>
    </div>
  );
}

export default App;
