import * as api from './creds.json';

const headers = { 'x-api-key': api.apikey };

async function post(url, body) {
    return fetch(url, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
    })
}

export function getSomething(something) {
}